-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 06 Juillet 2017 à 20:10
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `vaporisvitrinev2`
--

-- --------------------------------------------------------

--
-- Structure de la table `page_home_informations`
--

DROP TABLE IF EXISTS `page_home_informations`;
CREATE TABLE IF NOT EXISTS `page_home_informations` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `attached_img` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`card_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `page_home_informations`
--

INSERT INTO `page_home_informations` (`card_id`, `attached_img`, `title`, `text`) VALUES
(0, 'pages/img/home_informations_odyssey.png', 'L''Odyssée !', 'Le monde de Vaporis est immense ! Partez à l’aventure, découvrez ses secrets et résolvez ses mystères au travers de quêtes épiques ! Des donjons immenses regorgeant de richesses vous attendent, la gloire est au rendez-vous !'),
(1, 'pages/img/home_informations_conquest.png', 'La Conquête !', 'Choisissez parmi les nombreuses classes proposées et développez votre personnage, son influence et son expertise ! Regroupez-vous avec vos amis et partez en quête de puissance et de conquête contre les autres joueurs ! '),
(2, 'pages/img/home_informations_influence.png', 'L''Influence !', 'Participez aux événements communautaires pour faire évoluer l’ensemble de l’univers ! Créez votre histoire, soumettez-la à l’équipe et voyez le monde qui vous entoure réagir face à la destinée que vous vous serez forgé !');

-- --------------------------------------------------------

--
-- Structure de la table `page_home_staff`
--

DROP TABLE IF EXISTS `page_home_staff`;
CREATE TABLE IF NOT EXISTS `page_home_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mc_username` varchar(255) NOT NULL,
  `vaporis_role` varchar(255) NOT NULL,
  `personnal_description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `page_home_staff`
--

INSERT INTO `page_home_staff` (`id`, `mc_username`, `vaporis_role`, `personnal_description`) VALUES
(0, 'Vanilr', 'kzeufb', 'ezfhbeurlbf'),
(1, 'zef', 'fezfezefz', 'zefzergte'),
(2, 'zef', 'fezfezefz', 'zefzergte'),
(3, 'Test', 'tetqs', 'utdfza');

-- --------------------------------------------------------

--
-- Structure de la table `page_news`
--

DROP TABLE IF EXISTS `page_news`;
CREATE TABLE IF NOT EXISTS `page_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_encoded` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `main_img` varchar(255) DEFAULT NULL,
  `content` longtext NOT NULL,
  `post_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
