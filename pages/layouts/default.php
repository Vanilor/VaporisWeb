<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta lang="fr">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="<?php echo $config['favicon']; ?>">
    <title>Vaporis</title>

    <!-- CSS files-->
    <link rel="stylesheet" type="text/css" href="pages/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="pages/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="pages/css/fullpage.min.css">
    <link rel="stylesheet" type="text/css" href="pages/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="pages/css/style.css">
    <link rel="stylesheet" type="text/css" href="pages/css/navbar.css">
    <link rel="stylesheet" type="text/css" href="pages/css/footer.css">

</head>


<body>

<nav class="navbar navbar-fixed-top navbar-inverse" id="navbar">
    <div class="container" id="vaporis_top_navbar">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" id="vaporis-logo" rel="home" href=""><img style="max-width:120px; margin-top: -7px;" src="<?php echo $config['logo']; ?>"></a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo $config['dynmapUrl']; ?>" target="_blank">Dynmap</a></li>
            </ul>
            <ul class="nav navbar-nav" id="vaporis_center_navbar">
                <li><a <?php if($url[0] == "home"){echo "class=\"nav-active\"";} ?> href="home" id="test">Accueil</a></li>
                <li><a <?php if($url[0] == "store"){echo "class=\"nav-active\"";} ?> title="En travaux...">Boutique</a></li>
                <li><a <?php if($url[0] == "forum"){echo "class=\"nav-active\"";} ?> title="En travaux...">Forum</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Liens utiles <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a <?php if($url[0] == "news"){echo "class=\"nav-active\"";} ?> href="news">Actualités</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a <?php if($url[0] == "trophees"){echo "class=\"nav-active\"";} ?> href="trophees">Trophées</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a <?php if($url[0] == "legals"){echo "class=\"nav-active\"";} ?> href="legals">Mentions légales</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php // IF PLAYER ISN'T LOGGED, DISPLAY "CONNEXION" AND "INSCRIPTION", ELSE, DISPLAY "MON COMPTE" ?>
                <li><a href="#" data-rel="login" class="poplight">Connexion</a></li>
                <li><a <?php if($url[0] == "register"){echo "class=\"nav-active\"";} ?> href="register">Inscription</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">(<img src="https://epicube.fr/head/X_Vanilor?" id="vaporis_player_head"> Vanilor | Mon compte<span class="caret"></span>)</a>
                    <ul class="dropdown-menu">
                        <li><a <?php if($url[0] == "account"){echo "class=\"nav-active\"";} ?> href="account">Gestion du compte</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a <?php if($url[0] == "alliances"){echo "class=\"nav-active\"";} ?> href="alliances">Mon alliance</a></li>
                        <li><a <?php if($url[0] == "my_trophees"){echo "class=\"nav-active\"";} ?> href="my_trophees">Mes trophées</a> </li>
                        <li><a <?php if($url[0] == "my_stats"){echo "class=\"nav-active\"";} ?> href="my_stats">Mes statistiques</a> </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Déconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div id="fullpage">
    <div id="page">

    <?php echo $page; ?>

        <div class="section fp-auto-height" id="section<?php echo $lastEntrie; ?>">
            <!--footer start from here-->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="footer-dv">
                            <div class="col-lg-4 col-md-4 col-sm-4 footerleft">
                                <div class="logofooter">Publicité</div>
                                <p></p>
                            </div>
                        </div>
                        <div class="footer_dv">
                        <div class="col-lg-4 col-md-4 col-sm-4 paddingtop-bottom">
                            <h6 class="heading7">Derniers Tweets</h6>
                                <?php //THIS PART OF CODE IS AN EXPLANATION THAT I WANT TO CREATE, BUT I LET YOU DO AT YOUR OWN TO OPTIMIZE IT, JUST KEEP $maxTweets = 3, THE TWITTER API IS INSTANCIATE IN lib/start_queries.php

                            /*$maxTweets = 3;
                            $i = 0;
                            foreach($tweetsCache as $tweet){

                                if(!empty($tweet['entities']['urls'])){
                                    $NBUrls = count($tweet['entities']['urls']);
                                    $text = $tweet['text'];

                                    for($r = 0; $r < $NBUrls; $r++) {
                                       if(isset($occurence)){ $text = $occurence; }
                                       $tweetWithoutUrls = str_replace($tweet['entities']['urls'][$r]['url'], '', $text);
                                       $occurence = $tweetWithoutUrls;
                                    }

                                    $tweet['text'] = $tweetWithoutUrls.='<br><a href="'.$tweet['entities']['urls'][0]['expanded_url'].'">'.$tweet['entities']['urls'][0]['display_url'].'</a>';
                                }

                                $d1 = new DateTime($tweet["created_at"], new DateTimeZone('Europe/Paris'));
                                $d2 = $d1->getTimestamp();
                                $date = null;
                                    if(date('Y') != strftime("%Y", $d2)) {
                                        $date = strftime("%#d %B %Y", $d2);
                                    } else {
                                        $date = strftime("%#d %B", $d2);
                                    }
                                echo '
                                <div class="message">
                                    <span class="text-message" style="font-size: 10px; word-wrap: break-word;">"'.$tweet["text"].'"</span>
                                </div>
                                <div class="date">
                                    <span class="text-date">'.$date.'</span>
                                </div><br>';

                                $i++; if($i === $maxTweets){ break; }
                            }
                            $i = 0;*/ ?>
                        </div>
                        </div>
                        <div class="footer_dv">
                        <div class="col-lg-4 col-md-4 col-sm-4 paddingtop-bottom">
                            <h6 class="heading7">CONTACT & R&Eacute;SEAUX SOCIAUX</h6>
                            <div class="post">
                                <p>Assistance générale : <span><a href="mailto:<?php echo $config['mail']['mainContact']; ?>"><?php echo $config['mail']['mainContact']; ?></a></span></p>
                                <p><span>&nbsp;</span><i class="fa fa-twitter fa-lg"><a style="font-weight: bold" href="https://twitter.com/vaporismc" target="_blank"> @VaporisMC</a></i></p>
                                <p><span>&nbsp;</span><i class="fa fa-facebook-official fa-lg"> <a style="font-weight: bold" href="https://www.facebook.com/Vaporis.MC.Astra/" target="_blank">VaporisMC</a></i></p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--footer start from here-->

            <div class="copyright">
                <div class="container">
                    <div class="col-md-6">
                        <p>© 2017 - Vaporis, Tous droits réservés</p>
                    </div>
                    <div class="col-md-6">
                        <ul class="bottom_ul">
                            <li></li>
                            <li>Créer par <a href="http://zirpoo.tk" target="_blank">Zirpoo</a> & <a href="http://vanilor.cf" target="_blank">Vanilor</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="login" class="popup_block">
    <h4>Connexion :</h4>
    <form method="POST" action="">
        <label for="mc_username">Nom d'utilisateur (pseudo Minecraft) :</label>
        <input type="text" class="form-control" name="mc_username" id="mc_username" /><br>
        <label for="vaporis_password">Mot de passe (<i class="fa fa-eye" title="Voir le mot de passe" id="view_password" aria-hidden="true"></i>) :</label>
        <input type="password" class="form-control" name="vaporis_password" id="vaporis_password" />
    </form>
</div>

<script type="text/javascript" src="pages/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="pages/js/loginModal.js"></script>
<script type="text/javascript" src="pages/js/mouseWheel.js"></script>
<script type = "text/javascript" src = "pages/js/scrolloverflow.min.js" ></script>
<script type = "text/javascript" src = "pages/js/fullpage.min.js" ></script>
<script type = "text/javascript" src = "pages/js/fullpage.extensions.min.js" ></script>
<script type = "text/javascript" src = "pages/js/scroll.js" ></script>
<script>

</script>
</body>
</html>
