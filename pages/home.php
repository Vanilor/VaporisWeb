<br><br><br>
    <div class="section fp-responsive" id="section0" style="background-image: url(<?php echo $bg_img[0]; ?>)">
        <div class="content">
            <img class="img-responsive center-block" src="<?php echo $config['logo']; ?>" />

            <div class="center-block" id="vaporis_desc">
                <h4 class="text-center embed-responsive-item"><?php echo $config['description']; ?></h4>
            </div>
            <div class="center-block" id="vaporis_ip">
                <h1 class="text-center">Rejoins-nous : <?php echo strtoupper($config['ip']); ?></h1>
            </div>
            <!--<div class="center-block" id="vaporis_connect_players">
                <h4 class="text-center">Déjà <?php echo "42"; ?>/<?php echo "72"; ?> joueurs connectés !</h4>
            </div>-->
        </div>
    </div>


    <div class="section fp-responsive" id="section1" style="background-image: url(<?php echo $bg_img[1]; ?>)">
        <div class="content fp-responsive">
            <div class="center-block" id="vaporis_informations">

                <img class="infos_img" src="<?php echo $informations[0]->attached_img ?>" />
                <div class="infos">
                    <h3><?php echo $informations[0]->title; ?></h3>
                    <p><?php echo $informations[0]->text ?></p>
                </div>
                <div class="clearfix"></div><br><br><br><br>

                <img class="infos_img infos_conquest" src="<?php echo $informations[1]->attached_img ?>" />
                <div class="infos">
                    <h3><?php echo $informations[1]->title; ?></h3>
                    <p><?php echo $informations[1]->text ?></p>
                </div>
                <div class="clearfix"></div><br><br><br><br>

                <img class="infos_img infos_influence" src="<?php echo $informations[2]->attached_img ?>" />
                <div class="infos">
                    <h3><?php echo $informations[2]->title; ?></h3>
                    <p><?php echo $informations[2]->text ?></p>
                </div>

            </div>
        </div>
    </div>

    <div class="section fp-responsive" id="section2" style="background-image: url(<?php echo $bg_img[2]; ?>)">
        <div class="content">
            <div class="container"><br><br>
                <div class="table-responsivee" id="staff_profiles">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Test</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <th>Test</th>
                            </tr>
                        </tbody>
                    </table>

                <?php
                foreach($staffMembers as $staffMember){
                    echo '
                 
               ';
        } ?>

                </div>
            </div>
        </div>
    </div>

    <div class="section fp-responsive" id="section3" style="background-image: url(<?php echo $bg_img[3]; ?>)">
        <div class="content">
            <div id="vaporis_support">

                    <div class="row support">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 support_cat">
                            <div class="db-wrapper">
                                <div class="db-pricing-eleven db-bk-color-one">
                                    <div class="price">
                                        <h3>Un problème ?</h3>
                                    </div>
                                    <ul>
                                        <li></li>
                                        <li><a href="mailto: <?php echo $config['mail']['mainContact']; ?>"><img src="pages/img/home_support_mail.png" class="support_img"> Envoyer un mail</a></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 support_cat support_cat_2">
                            <div class="db-wrapper">
                                <div class="db-pricing-eleven db-bk-color-two">
                                    <div class="price">
                                        <h3>Restez connecté !</h3>
                                    </div>
                                    <ul>
                                        <li><a href="https://discordapp.com/invite/5Gy4ctd" title="Cliquez pour nous rejoindre !"><img src="pages/img/home_support_discord.png" class="support_img"> Discord </a></li>
                                        <li><a href="https://www.facebook.com/Vaporis.MC.Astra/" title="Cliquez pour nous rejoindre !"><img src="pages/img/home_support_facebook.png" class="support_img"> Notre page Facebook</a></li>
                                        <li><a href="https://twitter.com/vaporismc" title="Cliquez pour nous rejoindre !"><img src="pages/img/home_support_twitter.png" class="support_img"> Notre twitter</a></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>