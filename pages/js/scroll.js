$(document).ready(function() {
    $('#fullpage').fullpage({
        scrollOverflow: true,
        scrollOverflowOptions: {
            click: true
        },
        scrollBar: true
    });
});