jQuery(function($){

    //Lorsque vous cliquez sur un lien de la classe poplight
    $('a.poplight').on('click', function() {
        var popID = $(this).data('rel'); //Trouver la pop-up correspondante
        var popWidth = "350px"; //Trouver la largeur
        var currentPop = $('#'+popID);

        //Apparition du fond - .css({'filter' : 'alpha(opacity=80)'}) pour corriger les bogues d'anciennes versions de IE
        $('body').append('<div id="fade"></div>');
        $('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();

        //Faire apparaitre la pop-up et ajouter le bouton de fermeture
        currentPop.css({ 'width': popWidth, 'display' : "block"}).prepend('' +
            '<a href="#" class="btn_close">X</a>' +
            '');

        //Récupération du margin, qui permettra de centrer la fenêtre - on ajuste de 80px en conformité avec le CSS
        var popMargTop = (currentPop.height() + 80) / 2;
        var popMargLeft = (currentPop.width() + 80) / 2;

        //Apply Margin to Popup
        currentPop.css({
            'margin-top' : -popMargTop,
            'margin-left' : -popMargLeft
        });
        return false;
    });

    //Close Popups and Fade Layer
    $('body').on('click', 'a.btn_close, #fade', function() { //Au clic sur le body...
            $('#fade , .popup_block').fadeOut(function () {
                $('#fade, a.btn_close').remove();
            }); //...ils disparaissent ensemble
        return false;
    });
});

$(document).ready(function() {

    var password_view = document.getElementById('view_password');
    var password_input = document.getElementById('vaporis_password');

    password_view.onclick = function () {

        if (password_input.type == "password") {
            password_view.setAttribute('class', "fa fa-eye");
            password_view.setAttribute('title', "Afficher le mot de passe");
            password_input.setAttribute('type', 'text');
        }

        else {
            password_view.setAttribute('class', "fa fa-eye-slash");
            password_view.setAttribute('title', "Masquer le mot de passe");
            password_input.setAttribute('type', 'password');
        }

        return false;
    };
});