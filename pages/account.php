<div class="section fp-responsive" id="section0" style="background-image: url(<?php echo $bg_img[0]; ?>">
    <div class="content">
        <div class="profile_nav">
            <h1>Vanilor | <a href="account" class="profile_link">Mon compte</a></h1>
        </div>
        <div class="profile">
            <img src="https://epicube.fr/head/X_Vanilor?" class="profile_head" alt="Vanilor head" />

            <div class="container-fluid profile_card">

                <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                    <div class="card">
                        <div class="card-block">
                            <a class="profile_link" href="#section1">
                                <img src="https://epicube.fr/head/X_Vanilor?" class="profile_card_img" />
                                <h5>Mon profil</h5>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                    <div class="card">
                        <div class="card-block">
                            <a class="profile_link" href="#section2">
                                <img src="pages/img/profile_banner.png" class="profile_card_img" />
                                <h5>Mon alliance</h5>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                    <div class="card">
                        <div class="card-block">
                            <a class="profile_link" href="#section3">
                                <img src="pages/img/profile_dragon.png" class="profile_card_img" />
                                <h5>Mes trophées</h5>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 mt-4)">
                    <div class="card">
                        <div class="card-block">
                            <a class="profile_link" href="#section4">
                                <img src="pages/img/profile_stats.png" class="profile_card_img" />
                                <h5>Mes statistiques</h5>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>