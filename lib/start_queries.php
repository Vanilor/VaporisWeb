<?php
include "database_poo.php";
//include "twitter-api/api.php";


setlocale(LC_TIME, 'fr_FR');
Database::add('127.0.0.1', 'vaporisvitrinev2', 'root', 'root'); // HOST, DBNAME, USERNAME, PASSWORD
Database::select('vaporisvitrinev2'); // Select your first database to use

$config = json_decode(file_get_contents(ROOT."/lib/config.json"), true)[0];

if($url[0] == "home"){
    $informations = Database::query("SELECT * FROM page_home_informations", "stdClass", false);
    $staffMembers = Database::query('SELECT * FROM page_home_staff', 'sdtclass', false);
}

$bgsFolder = ROOT."/pages/img/bgs";
$errorPage = ROOT . "/pages/error.php";
$pageToLoad = ROOT."/pages/".$url[0].".php";

if(is_dir($bgsFolder)) {
    $bgsFolder = scandir($bgsFolder);
    $bgs = [];

    foreach($bgsFolder as $img) {
        if($img != "." && $img != "..") {
            array_push($bgs, $img);
        }
    }

} else {
    throw new Exception("Dossier bgs introuvable");
}

$load = null;
if(is_file($pageToLoad)) {
    $load = file_get_contents($pageToLoad);
} else { $error = 1; }

$bg_number = substr_count($load, "class=\"section");

$bg_img = [];

//DETERMINED THE NUMBER OF BACKGROUNDS THE SCRIPT HAVE TO GENERATE (PLEASE OPTIMIZE IT IF NECESSARY)
for($i = 0; $i < $bg_number; $i++){ $bg_img[$i] = 'pages/img/bgs/'.$bgs[rand(0, count($bgs)-1)]; }
$lastEntrie = $i;
$i = 0;
if(isset($error)){
    $bg_img[0] = 'pages/img/404.jpg';
}
