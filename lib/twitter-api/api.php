<?php
require_once('TwitterAPIExchange.php');
define("ROOTTwitter", str_replace("\\", "/", dirname(__DIR__)));

$settings = array(
    'oauth_access_token' => "818380799561109505-o2YDfTeEKL65CDebw6BlskpuRwyrYgl",
    'oauth_access_token_secret' => "AzSHHM5049GTuWpyEHGAnmK5fBGOp7Jv3FUndbUSuUtGN",
    'consumer_key' => "TKPL4R1MLnNAviced6s77ofYn",
    'consumer_secret' => "N8AhqwTmnr2IwAYF2Ds8SXsNrTUmktIUeWJAuPuRtXwLf821gU"
);

$urlForTweetsDatas = 'https://api.twitter.com/1.1/statuses/user_timeline/vaporismc.json';
$urlForVaporisDatas = "https://api.twitter.com/1.1/users/show/vaporismc.json"; //A supprimer dans une V2 du script

$getfield = '?count=1'; //Variable par défaut pour la première requête, ne pas toucher
$requestMethod = 'GET';
$cache = ROOTTwitter."/twitter-api/tweet_cache.json"; //Définition du nom du fichier de cache

if(!is_file($cache)){
    curl_file_create($cache);
    file_put_contents($cache, "{}");
}
$tweetsCache = json_decode(file_get_contents($cache), true); //Création si nécessaire et appel du fichier de cache

$twitter = new TwitterAPIExchange($settings);

$vaporisDatas = json_decode($twitter->setGetfield($getfield)
    ->buildOauth($urlForVaporisDatas, $requestMethod)
    ->performRequest(),true); //Première requête, sert uniquement à récupérer le nombre de tweets sans utiliser le fichier statuses/user_timeline de l'API

if(isset($vaporisDatas['errors'])){
    echo "Utilisation du fichier de cache (Erreur dans la requête pour récupérer le nombre de tweets)"; //Si il y a une erreur venant de Twitter (Ex: Rate Limit), on utilise les données qu'on a déjà en cache
    exit();
}

if($vaporisDatas['statuses_count'] > count($tweetsCache)){

    $dif = $vaporisDatas['statuses_count'] - count($tweetsCache);
    $getfield = '?count='.$dif;
    $vaporisTweets = json_decode($twitter->setGetfield($getfield)
        ->buildOauth($urlForTweetsDatas, $requestMethod)
        ->performRequest(),true);


    if(isset($vaporisTweets["errors"])){
        echo "Utilisation du fichier de cache (Erreur dans la requête pour récupérer les tweets)"; //Si il y a une erreur venant de Twitter (Ex: Rate Limit), on utilise les données qu'on a déjà en cache
        exit();
    }

    $i = 0;
    foreach($vaporisTweets as $tweet) { //On récupère les tweets que renvoi l'API, puis on les insère dans le tableau $tweetsCache

            if (!isset($tweetsCache[$i]['id_str']) || $tweetsCache[$i]['id_str'] != $tweet['id_str'] || empty($tweetsCache[$i]['id_str'])) {
                array_push($tweetsCache, $tweet);
                if(!isset($cacheChange)) {
                    $cacheChange = true;
                }
            }
        $i++;
    }
    $i = 0;
    if(isset($cacheChange)) {
        file_put_contents($cache, json_encode($tweetsCache, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }
}

elseif($vaporisDatas['statuses_count'] < count($tweetsCache)) { //Si $vaporisDatas['statuses_count'] < count($tweetsCache), ça veut dire qu'un tweet a été supprimé, donc on régénère le cache pour ne pas conserver le tweet supprimé sur le site

    $getfield = '?count='.$vaporisDatas['statuses_count'];
    $vaporisTweets = json_decode($twitter->setGetfield($getfield)
        ->buildOauth($urlForTweetsDatas, $requestMethod)
        ->performRequest(),true);

    if(isset($vaporisTweets["errors"])){
        echo "Erreur dans la récupération des tweets, régénération du cache impossible, veuillez réessayer dans 5 à 15 minutes";
        exit();
    }

    unlink($cache);

    curl_file_create($cache);
    file_put_contents($cache, "{}");
    $tweetsCache = [];

    foreach($vaporisTweets as $tweet) { //On récupère tous les tweets que renvoi l'API, puis on les insère dans le tableau $tweetsCache
        array_push($tweetsCache, $tweet);
    }

    file_put_contents($cache, json_encode($tweetsCache, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
}

$tweetsCache = json_decode(file_get_contents($cache), true); //Rappel du fichier de cache après update par l'API

uasort($tweetsCache, function ($a, $b) {
    if($a['id_str'] === $b['id_str']) return 0;
    return $a['id_str'] < $b['id_str'] ? -1 : 1;
});
$tweetsCache = array_reverse($tweetsCache);