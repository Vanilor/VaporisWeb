<?php
class Database {

	private static $databases = [];
	public static $database;
	public static $config;

	public function __construct() {}

	public static function add($host, $name, $user, $pass, $port = 3306) {
		if(!isset(self::$databases[$name])) {
			if(!empty($host) && !empty($name) && !empty($user)) {
				$data = (object)array(
					"host" => $host, 
					"port" => $port, 
					"name" => $name, 
					"user" => $user, 
					"pass" => $pass
				);

				self::$databases[$name] 	= $data;
				self::$databases[$name]->db = self::connect($data);
			}
		}
	}

	private static function connect($data) {
		try {
			return new PDO('mysql:host='.$data->host.';port='.$data->port.';dbname='.$data->name.'', $data->user, $data->pass, 
				array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); // PDO options

		} catch (PDOException $e) {
			return utf8_encode(trim($e->getMessage()));
		}
	}

	public static function get() {
		if(is_object(self::$database->db)) {
			return self::$database->db;
		}
	}

	public static function remove($name) {
		unset(self::$databases[$name]);
	}

	public static function select($name) {
		if(isset(self::$databases[$name])) {
			self::$database = self::$databases[$name];
		}
	}

	public static function setConfig($config) {
		self::$config = $config;
	}

	public static function getConfig() {
		return self::$config;
	}

	public static function query($statement, $class_name, $once = false) {
		$req = self::get()->query($statement);
		$req->setFetchMode(PDO::FETCH_CLASS, $class_name);
		
		if($once) {
			$results = $req->fetch();

		} else {
			$results = $req->fetchALL();
		}

		return $results;
	}

	public static function simpleQuery($statement) {
		self::get()->query($statement);
	}

	public static function queryArraysIndex($statement, $index, $class_name) {
		$req = self::get()->query($statement);
		$req->setFetchMode(PDO::FETCH_CLASS, $class_name);
		$results = $req->fetchALL();
		$array  = array();

		foreach($results as $result) {
			$array[$result->$index] = $result;
		}

		return $array;
	}

	public static function prepare($statement, $params, $class_name, $once = false) {
		$req = self::get()->prepare($statement);
		$req->execute($params);
		$req->setFetchMode(PDO::FETCH_CLASS, $class_name);
		if($once) {
			$results = $req->fetch();

		} else {
			$results = $req->fetchALL();
		}

		return $results;
	}

	public static function insert($statement, $params) {
		$req = self::get()->prepare($statement);
		$req->execute($params);
	}

	public static function insertReturnID($statement, $params) {
		$req = self::get()->prepare($statement);
		$req->execute($params);

		return self::get()->lastInsertId();
	}

	public static function update($statement, $params) {
		$req = self::get()->prepare($statement);
		$req->execute($params);
	}

	public static function delete($statement, $params) {
		$req = self::get()->prepare($statement);
		if(!empty($params)) {
			$req->execute($params);
		}
	}
}
?>