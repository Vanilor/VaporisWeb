<?php
define('ROOT', __DIR__);

error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();

date_default_timezone_set('Europe/Paris');

if(!isset($_GET['url']) || empty($_GET['url'])) {
    $url = array("home");

} else {
    $url = explode('/', htmlspecialchars($_GET['url']));
}

include ROOT.'/lib/start_queries.php'; //USING A START_QUERIES FILE BECAUSE OF THE USE OF VARIABLES IN PAGES CONTROLLERS THAT ARE PREVIOUSLY CREATED IN THE default.php FILE, SO THEY DON'T EXIST IN CONTROLLERS BECAUSE IT'S INCLUDED FIRST

ob_start();

if(is_file($pageToLoad)) {
    include $pageToLoad;

} else {
    include $errorPage;
}

$page = ob_get_clean();

require ROOT."/pages/layouts/default.php";
?>